package pm.c7.utils.hud;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Formatting;
import pm.c7.utils.BlanketConfig;
import pm.c7.utils.hud.helper.Enums;
import pm.c7.utils.hud.helper.InventoryHelper;
import pm.c7.utils.hud.helper.ItemDiff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class PickupLog {
    public static PickupLog INSTANCE = new PickupLog();

    private final MinecraftClient client;
    private final InventoryHelper helper;
    private BlanketConfig config;

    private int timerTick = 1;

    public PickupLog() {
        this.helper = new InventoryHelper();
        this.client = MinecraftClient.getInstance();
    }

    public void doTick(MinecraftClient client) {
        timerTick++;

        if (client != null) {
            if (timerTick % 5 == 0) {
                ClientPlayerEntity player = client.player;
                if (player != null) {
                    if (BlanketConfig.Features.PICKUP_LOG.getBooleanValue()) {
                        // bad
                        int invSize = 0;
                        invSize += player.inventory.main.size();
                        invSize += player.inventory.armor.size();
                        invSize += player.inventory.offHand.size();

                        ItemStack[] combinedInventory = new ItemStack[invSize];
                        for (ItemStack stack : player.inventory.main) {
                            combinedInventory[player.inventory.main.indexOf(stack)] = stack;
                        }
                        for (ItemStack stack : player.inventory.armor) {
                            combinedInventory[player.inventory.armor.indexOf(stack) + player.inventory.main.size()] = stack;
                        }
                        for (ItemStack stack : player.inventory.offHand) {
                            combinedInventory[player.inventory.offHand.indexOf(stack) + player.inventory.main.size() + player.inventory.armor.size()] = stack;
                        }

                        helper.getInventoryDifference(combinedInventory);
                    }
                }

                helper.cleanUpPickupLog();
            } else if (timerTick > 20) {
                timerTick = 1;
            }
        }
    }

    private static final List<ItemDiff> DUMMY_PICKUP_LOG = new ArrayList<>(Arrays.asList(new ItemDiff("Dirt", 128), new ItemDiff("Cobblestone", -64), new ItemDiff("Diamond Sword", 1)));

    public void drawItemPickupLog(float scale, boolean editMode, MatrixStack matrixStack) {
        float x = BlanketConfig.PickupLog.getActualX();
        float y = BlanketConfig.PickupLog.getActualY();

        Enums.AnchorPoint anchorPoint = BlanketConfig.PickupLog.getAnchorPoint();
        boolean downwards = anchorPoint == Enums.AnchorPoint.TOP_RIGHT || anchorPoint == Enums.AnchorPoint.TOP_LEFT;

        int height = 8 * 3;
        int width = client.textRenderer.getWidth("+ 1x Forceful Ember Chestplate");
        x -= Math.round(width * scale / 2);
        y -= Math.round(height * scale / 2);
        x /= scale;
        y /= scale;
        int intX = Math.round(x);
        int intY = Math.round(y);
        /*if (editMode) {
            int boxXOne = intX - 4;
            int boxXTwo = intX + width + 4;
            int boxYOne = intY - 4;
            int boxYTwo = intY + height + 4;
            buttonLocation.checkHoveredAndDrawBox(boxXOne, boxXTwo, boxYOne, boxYTwo, scale);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        }*/
        int i = 0;
        Collection<ItemDiff> log = helper.getItemPickupLog();
        if (editMode) {
            log = DUMMY_PICKUP_LOG;
        }
        for (ItemDiff itemDiff : log) {
            String text = String.format("%s %sx \u00a7r%s", itemDiff.getAmount() > 0 ? "\u00a7a+" : "\u00a7c-",
                    Math.abs(itemDiff.getAmount()), itemDiff.getDisplayName());
            int stringY = intY + (i * client.textRenderer.fontHeight);
            if (!downwards) {
                stringY = intY - (i * client.textRenderer.fontHeight);
                stringY += 18;
            }

            RenderSystem.enableBlend();
            client.textRenderer.drawWithShadow(matrixStack, text, intX, stringY, Formatting.WHITE.getColorValue());
            i++;
        }
    }
}
