package pm.c7.utils.hud;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.util.math.MatrixStack;
import pm.c7.utils.BlanketConfig;
import pm.c7.utils.hud.helper.Degree;

import java.awt.*;
import java.util.List;

public class CompassHUD {
    public static CompassHUD INSTANCE = new CompassHUD();
    private MinecraftClient client;
    private TextRenderer textRenderer;

    public List<Degree> degrees = Lists.newArrayList();

    public CompassHUD() {
        this.client = MinecraftClient.getInstance();
        this.textRenderer = this.client.textRenderer;

        degrees.add(new Degree("N", 1));
        degrees.add(new Degree("195", 2));
        degrees.add(new Degree("210", 2));
        degrees.add(new Degree("NE", 3));
        degrees.add(new Degree("240", 2));
        degrees.add(new Degree("255", 2));
        degrees.add(new Degree("E", 1));
        degrees.add(new Degree("285", 2));
        degrees.add(new Degree("300", 2));
        degrees.add(new Degree("SE", 3));
        degrees.add(new Degree("330", 2));
        degrees.add(new Degree("345", 2));
        degrees.add(new Degree("S", 1));
        degrees.add(new Degree("15", 2));
        degrees.add(new Degree("30", 2));
        degrees.add(new Degree("SW", 3));
        degrees.add(new Degree("60", 2));
        degrees.add(new Degree("75", 2));
        degrees.add(new Degree("W", 1));
        degrees.add(new Degree("105", 2));
        degrees.add(new Degree("120", 2));
        degrees.add(new Degree("NW", 3));
        degrees.add(new Degree("150", 2));
        degrees.add(new Degree("165", 2));
    }

    public void render(MatrixStack matrixStack) {
        RenderSystem.disableAlphaTest();
        RenderSystem.enableBlend();

        float center = this.client.getWindow().getScaledWidth() / 2.0f;

        int count = 0;
        int cardinals = 0;
        int subCardinals = 0;
        int markers = 0;
        float yaw = this.client.player != null ? (this.client.player.yaw % 360) * 2 + 360 * 3 : 0;

        for (int i = 0; i < 3; i++) {
            for (Degree d : degrees) {
                float location = center + (count * 30) - yaw;
                float completeLocation = location - this.textRenderer.getWidth(d.text) / 2.0F;

                Color ccolor = Color.getHSBColor(BlanketConfig.Features.COMPASS_RAINBOW.getBooleanValue() ? ((System.nanoTime() * BlanketConfig.Features.COMPASS_RAINBOW_SPEED.getIntegerValue())/9E9F + ((count - 1) * 0.01F)) % 1F : BlanketConfig.Features.COMPASS_HUE.getIntegerValue()/360F,BlanketConfig.Features.COMPASS_SATURATION.getIntegerValue()/100F, BlanketConfig.Features.COMPASS_VALUE.getIntegerValue()/100F);
                Color ocolor = opacity(ccolor.getRed(), ccolor.getGreen(), ccolor.getBlue(), this.client.getWindow().getScaledWidth(), completeLocation);
                int alpha = ocolor.getAlpha();
                int opacity = ocolor.getRGB();

                if (d.type == 1 && alpha > 3) {
                    RenderSystem.color4f(1, 1, 1, 1);

                    RenderSystem.pushMatrix();
                    RenderSystem.scalef(2.0F, 2.0F, 2.0F);
                    this.textRenderer.drawWithShadow(matrixStack, d.text, (int)completeLocation >> 1, (int)(25  - this.textRenderer.fontHeight / 2.0F) >> 1, opacity);
                    RenderSystem.scalef(0.5F, 0.5F, 0.5F);
                    RenderSystem.popMatrix();

                    cardinals++;
                }

                if (d.type == 2 && alpha > 3) {
                    RenderSystem.color4f(1, 1, 1, 1);
                    DrawableHelper.fill(matrixStack, (int)(location - 0.5F), -75 + 100 + 4, (int)(location + 0.5F), -75 + 105 + 4, opacity);
                    RenderSystem.color4f(1, 1, 1, 1);

                    RenderSystem.pushMatrix();
                    RenderSystem.scalef(0.5F, 0.5F, 0.5F);
                    this.textRenderer.drawWithShadow(matrixStack, d.text, (int)(location - (this.textRenderer.getWidth(d.text) / 2.0F) / 2.0F) << 1, (int)(37.5F) << 1, opacity);
                    RenderSystem.scalef(2.0F, 2.0F, 2.0F);
                    RenderSystem.popMatrix();

                    markers++;
                }

                if (d.type == 3 && alpha > 3) {
                    RenderSystem.color4f(1, 1, 1, 1);
                    this.textRenderer.drawWithShadow(matrixStack, d.text, completeLocation, 25 + this.textRenderer.fontHeight / 2.0f - this.textRenderer.fontHeight / 2.0f, opacity);
                    subCardinals++;
                }

                count++;
            }
        }
    }

    private Color opacity(int r, int g, int b, float resolution, float offset) {
        float offs = 255 - Math.abs(resolution / 2.0f - offset) * 1.8f;
        Color c = new Color(r, g, b, (int) Math.min(Math.max(0, offs), 255));

        return c;
    }
}
