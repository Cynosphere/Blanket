package pm.c7.utils.hud.helper;

import fi.dy.masa.malilib.config.IConfigOptionListEntry;

public class Enums {
    public enum AnchorPoint implements IConfigOptionListEntry {
        TOP_LEFT(0),
        TOP_RIGHT(1),
        BOTTOM_LEFT(2),
        BOTTOM_RIGHT(3),
        BOTTOM_MIDDLE(4);

        private int id;

        AnchorPoint(int id) {
            this.id = id;
        }

        public int getX(int maxX) {
            int x;
            switch (this) {
                case TOP_RIGHT:
                case BOTTOM_RIGHT:
                    x = maxX;
                    break;
                case BOTTOM_MIDDLE:
                    x = maxX / 2;// - 91;
                    break;
                default: // or case TOP_LEFT: case BOTTOM_LEFT:
                    x = 0;

            }
            return x;
        }

        public int getY(int maxY) {
            int y;
            switch (this) {
                case BOTTOM_LEFT: case BOTTOM_RIGHT: case BOTTOM_MIDDLE:
                    y = maxY;
                    break;
                default: // or case TOP_LEFT: case TOP_RIGHT:
                    y = 0;
            }
            return y;
        }

        @Override
        public String getStringValue() {
            switch (this) {
                case TOP_LEFT:
                    return "topLeft";
                case TOP_RIGHT:
                    return "topRight";
                case BOTTOM_LEFT:
                    return "bottomLeft";
                case BOTTOM_RIGHT:
                    return "bottomRight";
                case BOTTOM_MIDDLE:
                    return "bottomMiddle";
                default:
                    return "unknown";
            }
        }

        @Override
        public String getDisplayName() {
            switch (this) {
                case TOP_LEFT:
                    return "Top Left";
                case TOP_RIGHT:
                    return "Top Right";
                case BOTTOM_LEFT:
                    return "Bottom Left";
                case BOTTOM_RIGHT:
                    return "Bottom Right";
                case BOTTOM_MIDDLE:
                    return "Bottom Middle";
                default:
                    return "???";
            }
        }

        @Override
        public IConfigOptionListEntry cycle(boolean forward) {
            int id = this.ordinal();
            if (forward) {
                ++id;
                if (id >= values().length) {
                    id = 0;
                }
            } else {
                --id;
                if (id < 0) {
                    id = values().length - 1;
                }
            }

            return values()[id % values().length];
        }

        @Override
        public IConfigOptionListEntry fromString(String string) {
            switch (string) {
                case "topLeft":
                    return TOP_LEFT;
                case "topRight":
                    return TOP_RIGHT;
                case "bottomLeft":
                    return BOTTOM_LEFT;
                case "bottomRight":
                    return BOTTOM_RIGHT;
                case "bottomMiddle":
                    return BOTTOM_MIDDLE;
                default:
                    return null;
            }
        }
    }
}
