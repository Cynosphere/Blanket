package pm.c7.utils.hud;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tag.FluidTags;

public class ArmorHUD {
    public static ArmorHUD INSTANCE = new ArmorHUD();
    private MinecraftClient client;
    private ItemRenderer itemRenderer;

    public ArmorHUD(){
        this.client = MinecraftClient.getInstance();
        this.itemRenderer = client.getItemRenderer();
    }

    public void render(MatrixStack matrixStack){
        int width = this.client.getWindow().getScaledWidth();
        int height = this.client.getWindow().getScaledHeight();

        PlayerEntity player = this.client.player;
        if (player != null && !player.isCreative() && !player.isSpectator()) {
            int center = width/2;
            int index = 0;
            int breath = player.getAir();
            int maxBreath = player.getMaxAir();

            EquipmentSlot[] types = new EquipmentSlot[]{
                    EquipmentSlot.HEAD,
                    EquipmentSlot.CHEST,
                    EquipmentSlot.LEGS,
                    EquipmentSlot.FEET
            };

            RenderSystem.enableRescaleNormal();
            RenderSystem.enableBlend();
            RenderSystem.defaultBlendFunc();
            //DiffuseLighting.enable();

            for (EquipmentSlot slot : types) {
                ItemStack stack = player.inventory.armor.get(slot.getEntitySlotId());
                int x = center+(91/2)-(5 * 8 - 12)+(16*index);
                int y = height-56;
                if (player.isSubmergedIn(FluidTags.WATER) || breath < maxBreath) y -= 10;

                this.itemRenderer.renderGuiItemIcon(stack, x, y);
                this.itemRenderer.renderGuiItemOverlay(this.client.textRenderer, stack, x, y);

                index++;
            }

            //DiffuseLighting.disable();
            RenderSystem.disableRescaleNormal();
            RenderSystem.disableBlend();
        }
    }
}
