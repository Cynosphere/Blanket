package pm.c7.utils.hud;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffectUtil;
import net.minecraft.item.ItemStack;
import net.minecraft.item.PotionItem;
import net.minecraft.potion.PotionUtil;
import net.minecraft.util.Formatting;

import java.util.List;
import java.util.Map;

public class HotbarEnchantments {
    public static HotbarEnchantments INSTANCE = new HotbarEnchantments();
    private MinecraftClient client;

    public HotbarEnchantments(){
        this.client = MinecraftClient.getInstance();
    }

    public void render(MatrixStack matrixStack) {
        int width = this.client.getWindow().getScaledWidth();
        int height = this.client.getWindow().getScaledHeight();
        TextRenderer textRenderer = this.client.textRenderer;

        ItemStack handStack = client.player.getMainHandStack();
        if (handStack != null && !handStack.isEmpty()) {
            String text = "";
            if (handStack.getItem() instanceof PotionItem) {
                text = getPotionString(handStack);
            } else {
                text = getEnchantString(handStack);
            }

            RenderSystem.pushMatrix();
            RenderSystem.scalef(0.5F, 0.5F, 0.5F);
            int y = height - 54;
            y -= textRenderer.fontHeight;
            y <<= 1;
            int x = width - (textRenderer.getWidth(text) >> 1);
            textRenderer.draw(matrixStack, text, x, y, 0xCCCCCC);
            RenderSystem.scalef(2.0F, 2.0F, 2.0F);
            RenderSystem.popMatrix();
        }
    }

    private String getPotionString(ItemStack stack) {
        List<StatusEffectInstance> effects = PotionUtil.getPotionEffects(stack);
        if (effects.isEmpty()) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        for (StatusEffectInstance effect : effects) {
            out.append(Formatting.BOLD);
            out.append(I18n.translate(effect.getTranslationKey()).substring(0,3).toUpperCase());
            out.append(" ");
            out.append(effect.getAmplifier() + 1);
            out.append(" (");
            out.append(StatusEffectUtil.durationToString(effect, 1.0F));
            out.append(") ");
        }
        return out.toString().trim();
    }

    private String getEnchantString(ItemStack stack) {
        StringBuilder out = new StringBuilder();
        Map<Enchantment, Integer> enchants = EnchantmentHelper.get(stack);
        for (Map.Entry<Enchantment, Integer> entry : enchants.entrySet()) {
            Enchantment enchant = entry.getKey();
            int lvl = entry.getValue();

            out.append(Formatting.BOLD);
            out.append(I18n.translate(enchant.getTranslationKey()).substring(0,3).toUpperCase());
            out.append(" ");
            out.append(lvl);
            out.append(" ");
        }
        return out.toString().trim();
    }
}
