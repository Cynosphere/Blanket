package pm.c7.utils.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.SliderWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.math.MathHelper;
import pm.c7.utils.BlanketConfig;
import pm.c7.utils.hud.PickupLog;

public class PickupLogPositioningScreen extends Screen {
    private Screen parent = null;

    private double min = 0.325;
    private double max = 2.0;
    private float step = 0.1F;

    private MinecraftClient client;

    public PickupLogPositioningScreen(Screen parent, MinecraftClient client) {
        super(new LiteralText(""));
        this.parent = parent;
        this.client = client;
    }

    protected void init() {
        addButton(new ButtonWidget((this.width/2) - (118/2), 4, 118, 20, new TranslatableText("gui.done"), onClick -> {
            this.client.openScreen(parent);
        }));

        SliderWidget slider = new SliderWidget((this.width / 2) - (118 / 2), 28, 118, 20, new LiteralText(""), getRatio(BlanketConfig.PickupLog.getGuiScale())) {
            @Override
            protected void updateMessage() {
                this.setMessage(new LiteralText(String.format("Scale: %.1f", getValue(this.value))));
            }

            @Override
            protected void applyValue() {
                BlanketConfig.PickupLog.setGuiScale((float) getValue(this.value));
            }
        };
        slider.setMessage(new LiteralText(String.format("Scale: %.1f", BlanketConfig.PickupLog.getGuiScale())));
        addButton(slider);
    }

    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float delta) {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, delta);

        float scale = BlanketConfig.PickupLog.getGuiScale();
        RenderSystem.pushMatrix();
        RenderSystem.scalef(scale, scale, 1);
        PickupLog.INSTANCE.drawItemPickupLog(scale, true, matrixStack);
        RenderSystem.popMatrix();
    }

    public boolean mouseDragged(double mouseX, double mouseY, int button, double deltaX, double deltaY) {
        int x = (int) (mouseX - BlanketConfig.PickupLog.getAnchorPoint().getX(this.client.getWindow().getScaledWidth()));
        int y = (int) (mouseY - BlanketConfig.PickupLog.getAnchorPoint().getY(this.client.getWindow().getScaledHeight()));

        BlanketConfig.PickupLog.setCoords(x, y);
        BlanketConfig.PickupLog.setClosestAnchorPoint();

        return super.mouseDragged(mouseX, mouseY, button, deltaX, deltaY);
    }

    public double getRatio(double value) {
        return MathHelper.clamp((this.adjust(value) - this.min) / (this.max - this.min), 0.0D, 1.0D);
    }

    public double getValue(double ratio) {
        return this.adjust(MathHelper.lerp(MathHelper.clamp(ratio, 0.0D, 1.0D), this.min, this.max));
    }

    private double adjust(double value) {
        value = this.step * (float)Math.round(value / (double)this.step);

        return MathHelper.clamp(value, this.min, this.max);
    }
}
