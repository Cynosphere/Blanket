package pm.c7.utils.gui;

import fi.dy.masa.malilib.config.IConfigBase;
import fi.dy.masa.malilib.gui.GuiConfigsBase;
import fi.dy.masa.malilib.gui.button.ButtonBase;
import fi.dy.masa.malilib.gui.button.ButtonGeneric;
import fi.dy.masa.malilib.gui.button.IButtonActionListener;
import fi.dy.masa.malilib.util.StringUtils;
import pm.c7.utils.Blanket;
import pm.c7.utils.BlanketConfig;

import java.util.Collections;
import java.util.List;

public class ConfigGui extends GuiConfigsBase {
    private static GuiTabs tab = GuiTabs.FEATURES;
    private static GuiTabs lastTab;

    public ConfigGui() {
        super(10, 50, Blanket.MOD_ID, null, "config.blanket.title");
    }

    @Override
    public void initGui() {
        super.initGui();
        this.clearOptions();

        int x = 10;
        int y = 26;

        for (GuiTabs tab : GuiTabs.values())
        {
            x += this.createButton(x, y, -1, tab);
        }
    }

    private int createButton(int x, int y, int width, GuiTabs tab) {
        ButtonGeneric button = new ButtonGeneric(x, y, width, 20, tab.getDisplayName());
        button.setEnabled(ConfigGui.tab != tab);
        this.addButton(button, new ButtonListener(tab, this));

        return button.getWidth() + 2;
    }

    @Override
    protected boolean useKeybindSearch() {
        return ConfigGui.tab == GuiTabs.KEYBINDS || ConfigGui.tab == GuiTabs.AUTOWALK;
    }

    @Override
    public List<ConfigOptionWrapper> getConfigs() {
        List<? extends IConfigBase> configs;
        GuiTabs tab = ConfigGui.tab;

        switch(tab) {
            case FEATURES:
                configs = BlanketConfig.Features.OPTIONS;
                break;
            case TITLEBAR:
                configs = BlanketConfig.Titlebar.OPTIONS;
                break;
            case AUTOWALK:
                configs = BlanketConfig.Autowalk.OPTIONS;
                break;
            case KEYBINDS:
                configs = BlanketConfig.Keybinds.HOTKEY_LIST;
                break;
            case PICKUPLOG:
                ConfigGui.tab = ConfigGui.lastTab;

                PickupLogPositioningScreen positioningScreen = new PickupLogPositioningScreen(this, this.mc);
                this.mc.openScreen(positioningScreen);

                return Collections.emptyList();
            default:
                return Collections.emptyList();
        }

        return ConfigOptionWrapper.createFor(configs);
    }

    private static class ButtonListener implements IButtonActionListener
    {
        private final ConfigGui parent;
        private final GuiTabs tab;

        public ButtonListener(GuiTabs tab, ConfigGui parent)
        {
            this.tab = tab;
            this.parent = parent;
        }


        @Override
        public void actionPerformedWithButton(ButtonBase button, int mouseButton)
        {
            ConfigGui.lastTab = ConfigGui.tab;
            ConfigGui.tab = this.tab;

            this.parent.reCreateListWidget(); // apply the new config width
            this.parent.getListWidget().resetScrollbarPosition();
            this.parent.initGui();
        }
    }

    public enum GuiTabs {
        FEATURES   ("config.blanket.features"),
        TITLEBAR   ("config.blanket.titlebar"),
        AUTOWALK   ("config.blanket.autowalk"),
        KEYBINDS   ("config.blanket.keybinds"),
        PICKUPLOG  ("config.blanket.pickupLogPos");

        private final String translationKey;

        GuiTabs(String translationKey) {
            this.translationKey = translationKey;
        }

        public String getDisplayName() {
            return StringUtils.translate(this.translationKey);
        }
    }
}
