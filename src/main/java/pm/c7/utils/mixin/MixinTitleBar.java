package pm.c7.utils.mixin;

import net.minecraft.SharedConstants;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.WindowEventHandler;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraft.util.snooper.Snooper;
import net.minecraft.util.snooper.SnooperListener;
import net.minecraft.util.thread.ReentrantThreadExecutor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import pm.c7.utils.BlanketConfig;

@Mixin(MinecraftClient.class)
public class MixinTitleBar extends ReentrantThreadExecutor<Runnable> implements SnooperListener, WindowEventHandler {
    @Shadow
    private IntegratedServer server;
    @Shadow
    private ServerInfo currentServerEntry;

    public MixinTitleBar(String name) {
        super(name);
    }

    @Inject(method = "getWindowTitle", at = @At("HEAD"), cancellable = true)
    private void betterTitleBar(CallbackInfoReturnable info) {
        StringBuilder title = new StringBuilder("Minecraft");
        if (this.isModded() && !BlanketConfig.Titlebar.HIDE_MODDED.getBooleanValue()) {
            title.append("*");
        }

        title.append(" ");
        title.append(SharedConstants.getGameVersion().getName());

        if (!BlanketConfig.Titlebar.CLASSIC_MODE.getBooleanValue()) {
            ClientPlayNetworkHandler clientPlayNetworkHandler = this.getNetworkHandler();
            if (clientPlayNetworkHandler != null && clientPlayNetworkHandler.getConnection().isOpen()) {
                title.append(" - ");

                if (this.server != null && !this.server.isRemote()) {
                    title.append(I18n.translate("title.singleplayer"));
                } else if (this.isConnectedToRealms()) {
                    title.append(I18n.translate("title.multiplayer.realms"));
                } else if (this.server == null && this.currentServerEntry != null && !this.currentServerEntry.isLocal() && BlanketConfig.Titlebar.ENHANCED_MODE.getBooleanValue()) {
                    title.append(this.currentServerEntry.name);
                    if (BlanketConfig.Titlebar.ENHANCED_SHOW_IP.getBooleanValue())
                        title.append(String.format(" (%s)", this.currentServerEntry.address));
                } else if (this.server == null && this.currentServerEntry != null && this.currentServerEntry.isLocal()){
                    title.append(I18n.translate("title.multiplayer.lan"));
                } else {
                    title.append(I18n.translate("menu.multiplayer"));
                }
            }
        }

        info.setReturnValue(title.toString());
        info.cancel();
    }

    @Shadow
    public ClientPlayNetworkHandler getNetworkHandler() {
        return null;
    }

    @Shadow
    public boolean isModded() {
        return true;
    }

    @Shadow
    private boolean isConnectedToRealms() {
        return false;
    }

    @Shadow
    public void onWindowFocusChanged(boolean focused) {}

    @Shadow
    public void onResolutionChanged() {}

    @Shadow
    public void method_30133() {}

    @Shadow
    public void addSnooperInfo(Snooper snooper) {}

    @Shadow
    protected Runnable createTask(Runnable runnable) {
        return null;
    }

    @Shadow
    protected boolean canExecute(Runnable task) {
        return false;
    }

    @Shadow
    protected Thread getThread() {
        return null;
    }
}
