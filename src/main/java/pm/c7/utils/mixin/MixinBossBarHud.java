package pm.c7.utils.mixin;

import net.minecraft.client.gui.hud.BossBarHud;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.c7.utils.BlanketConfig;

@Mixin(BossBarHud.class)
public class MixinBossBarHud {
    @Inject(method="render",at=@At(value="HEAD"),cancellable=true)
    private void noBossBar(CallbackInfo info){
        if(BlanketConfig.Features.NO_BOSS_BAR.getBooleanValue()) {
            info.cancel();
        }
    }
}
