package pm.c7.utils.mixin.fullbright;

import net.minecraft.client.render.GameRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import pm.c7.utils.BlanketConfig;

@Mixin(GameRenderer.class)
public class MixinGameRenderer {
    @ModifyConstant(method = "renderWorld", constant = {@Constant(intValue = 20), @Constant(intValue = 7)})
    public int preventNauseaEffect(int orig) {
        return BlanketConfig.Features.ANTI_BLINDNESS.getBooleanValue() ? 0 : orig;
    }
}
