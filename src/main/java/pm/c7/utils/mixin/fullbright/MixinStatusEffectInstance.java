package pm.c7.utils.mixin.fullbright;

import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import pm.c7.utils.BlanketConfig;

@Mixin(StatusEffectInstance.class)
public abstract class MixinStatusEffectInstance {
    @Shadow public abstract StatusEffect getEffectType();

    @Inject(method = "getDuration", at = @At(value = "HEAD"), cancellable = true)
    private void doFullbright(CallbackInfoReturnable info){
        StatusEffect effect = ((StatusEffectInstance)(Object)this).getEffectType();
        if (BlanketConfig.Internals.FULLBRIGHT_ENABLED.getBooleanValue() && effect == StatusEffects.NIGHT_VISION) {
            info.setReturnValue(999999);
            return;
        }
    }
}
