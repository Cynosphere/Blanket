package pm.c7.utils.mixin.fullbright;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import pm.c7.utils.BlanketConfig;

@Mixin(LivingEntity.class)
public class MixinLivingEntity {
    @Inject(method = "hasStatusEffect", at = @At(value = "HEAD"), cancellable = true)
    private void doFullbright(StatusEffect effect, CallbackInfoReturnable info){
        if (BlanketConfig.Internals.FULLBRIGHT_ENABLED.getBooleanValue() && effect == StatusEffects.NIGHT_VISION) {
            info.setReturnValue(true);
            return;
        }

        if (BlanketConfig.Features.ANTI_BLINDNESS.getBooleanValue() && effect == StatusEffects.BLINDNESS) {
            info.setReturnValue(false);
            info.cancel();
        }
    }

    @Inject(method = "getStatusEffect", at = @At(value = "HEAD"), cancellable = true)
    private void getStatusEffectFix(StatusEffect effect, CallbackInfoReturnable info){
        if (BlanketConfig.Internals.FULLBRIGHT_ENABLED.getBooleanValue() && effect == StatusEffects.NIGHT_VISION) {
            info.setReturnValue(new StatusEffectInstance(StatusEffects.NIGHT_VISION,99999));
            return;
        }
    }
}
