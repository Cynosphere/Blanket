package pm.c7.utils.mixin;

import io.github.prospector.modmenu.ModMenu;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.screen.GameMenuScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.AbstractButtonWidget;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.List;

// https://github.com/Minenash/Better-Mod-Button/blob/1.16/src/main/java/com/minenash/bettermodbutton/mixin/ScreenMixin.java
@Mixin(Screen.class)
public abstract class MixinScreenModMenu {
    @Shadow
    public int height;
    @Shadow public int width;
    @Shadow protected abstract <T extends AbstractButtonWidget> T addButton(T button);
    @Shadow @Final
    protected List<Element> children;
    @Shadow @Final protected List<AbstractButtonWidget> buttons;

    @Inject(at = @At("HEAD"), method = "addButton", cancellable = true)
    public void moveButtons(AbstractButtonWidget button, CallbackInfoReturnable info) {
        Text message = button.getMessage();

        if ((Object)this instanceof GameMenuScreen) {
            if (message.equals(new TranslatableText("modmenu.title").append(new LiteralText(" ")).append(new TranslatableText("modmenu.loaded", ModMenu.getDisplayedModCount())))) {
                if (button.getWidth() != 98) {
                    button.x = this.width / 2 + 4;
                    button.y = this.height / 4 + 60 + -16;
                    button.setWidth(98);
                    button.setMessage(new TranslatableText("modmenu.title"));
                }
            }
        }
    }
}
