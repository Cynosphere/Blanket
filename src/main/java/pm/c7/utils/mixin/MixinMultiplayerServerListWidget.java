package pm.c7.utils.mixin;

import net.minecraft.SharedConstants;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerServerListWidget;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

@Mixin(MultiplayerServerListWidget.ServerEntry.class)
public class MixinMultiplayerServerListWidget {
    @Final
    @Shadow
    private ServerInfo server;

    @ModifyVariable(method = "render", at = @At("STORE"), ordinal = 0)
    private Text showVersionInfo(Text orig) {
        boolean older = this.server.protocolVersion > SharedConstants.getGameVersion().getProtocolVersion();
        boolean newer = this.server.protocolVersion < SharedConstants.getGameVersion().getProtocolVersion();
        boolean outdated = older || newer;

        if (this.server.online && this.server.ping > 0L) {
            return new LiteralText((outdated ? Formatting.DARK_RED.toString() : Formatting.AQUA.toString()) + this.server.version.getString() + Formatting.DARK_GRAY + " | " + Formatting.GRAY + this.server.playerCountLabel.getString());
        }

        return orig;
    }
}
