package pm.c7.utils.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.gui.screen.GameMenuScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.c7.utils.Blanket;
import pm.c7.utils.gui.ConfigGui;

@Mixin(GameMenuScreen.class)
public class MixinGameMenuScreen extends Screen {
    protected MixinGameMenuScreen(Text title) {
        super(title);
    }

    MinecraftClient minecraft = MinecraftClient.getInstance();

    ButtonWidget btnReconnect;

    @Inject(at = @At("RETURN"), method = "initWidgets()V")
    private void addReconnectButton(CallbackInfo info) {
        this.btnReconnect = this.addButton(new ButtonWidget(4, 4, 98, 20, new LiteralText("Reconnect"), (widget) -> {
            if (this.minecraft.world != null) {
                this.minecraft.world.disconnect();
                ServerInfo entry = new ServerInfo("", Blanket.lastIp + ":" + Blanket.lastPort, false);
                this.minecraft.openScreen(new ConnectScreen(this, this.minecraft, entry));
            }
        }));
        this.btnReconnect.y = 4;
        this.btnReconnect.active = !this.minecraft.isIntegratedServerRunning();
    }

    @Inject(at = @At("RETURN"), method = "tick")
    private void updateReconnect(CallbackInfo info) {
        if (this.btnReconnect != null) {
            this.btnReconnect.active = !this.minecraft.isIntegratedServerRunning();
        }
    }
}
