package pm.c7.utils.mixin.reconnect;

import net.minecraft.client.gui.screen.ConnectScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.c7.utils.Blanket;

@Mixin(ConnectScreen.class)
public class MixinConnectScreen {
    @Inject(method = "connect", at = @At("HEAD"))
    private void storeConnection(String ip, int port, CallbackInfo info) {
        Blanket.lastIp = ip;
        Blanket.lastPort = port;
    }
}
