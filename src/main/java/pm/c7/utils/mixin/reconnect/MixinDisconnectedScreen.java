package pm.c7.utils.mixin.reconnect;

import net.minecraft.class_5489;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.gui.screen.DisconnectedScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.c7.utils.Blanket;

@Mixin(DisconnectedScreen.class)
public abstract class MixinDisconnectedScreen extends Screen {
    @Shadow
    private class_5489 reasonFormatted;
    @Shadow
    private int reasonHeight;
    @Shadow
    private Text reason;
    @Shadow
    private Screen parent;

    protected MixinDisconnectedScreen(Text title) {
        super(title);
    }

    @Inject(method = "init", at = @At("RETURN"))
    private void addReconnect(CallbackInfo info) {
        this.reasonFormatted = class_5489.method_30890(this.textRenderer, this.reason, this.width - 50);
        int reasonSize = this.reasonFormatted.method_30887();
        this.reasonHeight = reasonSize * 9;
        int w = this.width / 2 - 100;
        int h = this.height / 2 + this.reasonHeight / 2;

        this.addButton(new ButtonWidget(w, Math.min(h + 9, this.height - 30) + 24, 200, 20, new LiteralText("Reconnect"), (buttonWidget) -> {
            reconnect();
        }));
    }

    private void reconnect() {
        ServerInfo entry = new ServerInfo("", Blanket.lastIp + ":" + Blanket.lastPort, false);
        this.client.openScreen(new ConnectScreen(this.parent, this.client, entry));
    }
}
