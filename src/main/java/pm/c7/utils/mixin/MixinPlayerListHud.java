package pm.c7.utils.mixin;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.hud.PlayerListHud;
import net.minecraft.client.network.PlayerListEntry;
import net.minecraft.client.util.math.MatrixStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.c7.utils.BlanketConfig;

@Mixin(PlayerListHud.class)
public class MixinPlayerListHud {
    @Shadow
    MinecraftClient client;

    @Inject(method = "renderLatencyIcon", at = @At("HEAD"), cancellable = true)
    private void numericPing(MatrixStack matrixStack, int x, int w, int y, PlayerListEntry playerEntry, CallbackInfo info){
        if (BlanketConfig.Features.TAB_LIST_PING.getBooleanValue()) {
            int color;

            if (playerEntry.getLatency() > 500) {
                color = 0xAA0000;
            } else if (playerEntry.getLatency() > 300) {
                color = 0xAAAA00;
            } else if (playerEntry.getLatency() > 200) {
                color = 0xAACC00;
            } else if (playerEntry.getLatency() > 135) {
                color = 0x207B00;
            } else if (playerEntry.getLatency() > 70) {
                color = 0x009900;
            } else if (playerEntry.getLatency() >= 0) {
                color = 0x00BB00;
            } else {
                color = 0xAA0000;
            }

            RenderSystem.pushMatrix();
            RenderSystem.scalef(0.5F, 0.5F, 0.5F);
            int yPos = y + (this.client.textRenderer.fontHeight / 4);
            yPos <<= 1;
            int xPos = w + x - 11;
            xPos <<= 1;

            this.client.textRenderer.drawWithShadow(matrixStack, playerEntry.getLatency() + "ms", xPos, yPos, color);
            RenderSystem.scalef(2.0F, 2.0F, 2.0F);
            RenderSystem.popMatrix();

            info.cancel();
        }
    }

    @Redirect(method = "render", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/MinecraftClient;isInSingleplayer()Z"))
    public boolean alwaysDrawPlayerIcons(MinecraftClient client) {
        return true;
    }
}
