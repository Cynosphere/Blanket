package pm.c7.utils.mixin;

import net.minecraft.client.gui.hud.InGameHud;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pm.c7.utils.BlanketConfig;

@Mixin(InGameHud.class)
public class MixinInGameHud {
    @Inject(method="renderStatusEffectOverlay",at=@At(value="HEAD"), cancellable=true)
    private void noStatusEffectOverlay(CallbackInfo info){
        if (BlanketConfig.Features.NO_EFFECT_HUD.getBooleanValue())
            info.cancel();
    }

    @Inject(method = "renderPumpkinOverlay", at = @At(value = "HEAD"), cancellable=true)
    private void noPumpkinOverlay(CallbackInfo info) {
        if (BlanketConfig.Features.NO_PUMPKIN_OVERLAY.getBooleanValue())
            info.cancel();
    }

    @Inject(method = "renderScoreboardSidebar", at = @At("HEAD"), cancellable = true)
    private void hideScoreboardTweak(CallbackInfo info) {
        if (BlanketConfig.Features.NO_SCOREBOARD.getBooleanValue())
            info.cancel();
    }

    @ModifyVariable(method = "renderScoreboardSidebar", at = @At("STORE"), ordinal = 0)
    private String scoreboardNumbersTweak(String orig) {
        if (BlanketConfig.Features.NO_SCOREBOARD_NUMBERS.getBooleanValue())
            return "";

        return orig;
    }
}
