package pm.c7.utils.modules;

import fi.dy.masa.malilib.util.InfoUtils;
import fi.dy.masa.malilib.util.StringUtils;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import pm.c7.utils.BlanketConfig;
import pm.c7.utils.mixin.KeyCodeAccessor;

// https://github.com/Draylar/presstowalk, gone from cf rip
public class AutowalkModule {
    public static void handleForcedResets(MinecraftClient client) {
        if (client.options.keyBack.wasPressed()) {
            if (BlanketConfig.Internals.AUTOWALK_ENABLED.getBooleanValue()) {
                InfoUtils.printActionbarMessage(StringUtils.translate("blanket.autowalk.stopped"));
                KeyBinding.setKeyPressed(getConfiguredKeyCode(client.options.keyForward), false);

                BlanketConfig.Internals.AUTOWALK_ENABLED.setBooleanValue(false);
            }
        }

        if (client.options.keyForward.wasPressed()) {
            if (BlanketConfig.Internals.AUTOWALK_ENABLED.getBooleanValue()) {
                InfoUtils.printActionbarMessage(StringUtils.translate("blanket.autowalk.stopped"));
                KeyBinding.setKeyPressed(getConfiguredKeyCode(client.options.keyForward), false);

                BlanketConfig.Internals.AUTOWALK_ENABLED.setBooleanValue(false);
            }
        }
    }

    public static void handleAutoWalk(MinecraftClient client) {
        if (BlanketConfig.Internals.AUTOWALK_ENABLED.getBooleanValue()) {
            // handle special mining auto-walk mechanics
            if (BlanketConfig.Internals.AUTOMINE_ENABLED.getBooleanValue()) {
                // make sure they're attempting to mine
                if (client.options.keyAttack.isPressed()) {
                    World world = client.world;
                    ClientPlayerEntity player = client.player;

                    // make sure we're not falling off a cliff
                    BlockPos offset = player.getBlockPos().offset(player.getHorizontalFacing()).offset(Direction.DOWN);
                    BlockState state = world.getBlockState(offset);
                    if (!state.isAir() && !isLiquid(state)) {
                        // don't hit our head
                        if (world.getBlockState(player.getBlockPos().offset(Direction.UP).offset(player.getHorizontalFacing(), 2)).isAir()) {
                            KeyBinding.setKeyPressed(getConfiguredKeyCode(client.options.keyForward), true);
                        } else {
                            resetPressedKeys(client);
                        }
                    } else {
                        resetPressedKeys(client);
                    }
                } else {
                    resetPressedKeys(client);
                }
            } else {
                KeyBinding.setKeyPressed(getConfiguredKeyCode(client.options.keyForward), true);

                // force sprint if the config option is toggled on
                if (BlanketConfig.Autowalk.FORCE_SPRINT.getBooleanValue()) {
                    KeyBinding.setKeyPressed(getConfiguredKeyCode(client.options.keySprint), true);
                }
            }
        }
    }

    public static void resetPressedKeys(MinecraftClient client) {
        KeyBinding.setKeyPressed(getConfiguredKeyCode(client.options.keyForward), false);
        KeyBinding.setKeyPressed(getConfiguredKeyCode(client.options.keySprint), false);
    }


    public static boolean isLiquid(BlockState state) {
        return state.getBlock() == Blocks.WATER || state.getBlock() == Blocks.LAVA;
    }

    private static InputUtil.Key getConfiguredKeyCode(KeyBinding keyBinding) {
        return ((KeyCodeAccessor) keyBinding).getConfiguredKeyCode();
    }
}
