package pm.c7.utils.event;

import fi.dy.masa.malilib.interfaces.IClientTickHandler;
import net.minecraft.client.MinecraftClient;
import pm.c7.utils.hud.PickupLog;
import pm.c7.utils.modules.AutowalkModule;

public class ClientTickHandler implements IClientTickHandler {
    @Override
    public void onClientTick(MinecraftClient client) {
        if (client.world != null && client.player != null) {
            AutowalkModule.handleAutoWalk(client);
            AutowalkModule.handleForcedResets(client);
        }

        PickupLog.INSTANCE.doTick(client);
    }
}
