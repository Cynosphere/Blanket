package pm.c7.utils.event;

import fi.dy.masa.malilib.hotkeys.*;
import pm.c7.utils.Blanket;
import pm.c7.utils.BlanketConfig;

public class InputHandler implements IKeybindProvider, IKeyboardInputHandler, IMouseInputHandler {
    private static final InputHandler INSTANCE = new InputHandler();

    private InputHandler()
    {
        super();
    }

    public static InputHandler getInstance()
    {
        return INSTANCE;
    }

    @Override
    public void addKeysToMap(IKeybindManager manager) {
        for (IHotkey hotkey : BlanketConfig.Keybinds.HOTKEY_LIST) {
            manager.addKeybindToMap(hotkey.getKeybind());
        }
        for (IHotkey hotkey : BlanketConfig.Autowalk.HOTKEY_LIST) {
            manager.addKeybindToMap(hotkey.getKeybind());
        }
    }

    @Override
    public void addHotkeys(IKeybindManager manager) {
        manager.addHotkeysForCategory(Blanket.MOD_NAME, "config.blanket.keybinds", BlanketConfig.Keybinds.HOTKEY_LIST);
        manager.addHotkeysForCategory(Blanket.MOD_NAME, "config.blanket.autowalk", BlanketConfig.Autowalk.HOTKEY_LIST);
    }
}
