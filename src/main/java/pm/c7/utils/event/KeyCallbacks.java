package pm.c7.utils.event;

import fi.dy.masa.malilib.gui.GuiBase;
import fi.dy.masa.malilib.hotkeys.IHotkeyCallback;
import fi.dy.masa.malilib.hotkeys.IKeybind;
import fi.dy.masa.malilib.hotkeys.KeyAction;
import fi.dy.masa.malilib.util.InfoUtils;
import fi.dy.masa.malilib.util.StringUtils;
import net.minecraft.client.MinecraftClient;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import pm.c7.utils.BlanketConfig;
import pm.c7.utils.gui.ConfigGui;
import pm.c7.utils.mixin.SoundSystemAccessor;
import pm.c7.utils.modules.AutowalkModule;

public class KeyCallbacks {
    public static void init(MinecraftClient client) {
        IHotkeyCallback callbackGeneric = new KeyCallbackHotkeysGeneric(client);

        BlanketConfig.Keybinds.OPEN_CONFIG_GUI.getKeybind().setCallback(callbackGeneric);
        BlanketConfig.Keybinds.FULLBRIGHT.getKeybind().setCallback(callbackGeneric);
        BlanketConfig.Keybinds.RELOAD_SOUND.getKeybind().setCallback(callbackGeneric);

        BlanketConfig.Autowalk.AUTOWALK_KEY.getKeybind().setCallback(callbackGeneric);
        BlanketConfig.Autowalk.AUTOMINE_KEY.getKeybind().setCallback(callbackGeneric);
    }

    private static class KeyCallbackHotkeysGeneric implements IHotkeyCallback
    {
        private final MinecraftClient client;

        public KeyCallbackHotkeysGeneric(MinecraftClient client)
        {
            this.client = client;
        }

        @Override
        public boolean onKeyAction(KeyAction action, IKeybind key) {
            if (key == BlanketConfig.Keybinds.OPEN_CONFIG_GUI.getKeybind()) {
                GuiBase.openGui(new ConfigGui());
                return true;
            } else if (key == BlanketConfig.Keybinds.FULLBRIGHT.getKeybind()) {
                BlanketConfig.Internals.FULLBRIGHT_ENABLED.setBooleanValue(!BlanketConfig.Internals.FULLBRIGHT_ENABLED.getBooleanValue());
                InfoUtils.printActionbarMessage(StringUtils.translate("blanket.message.value." + (BlanketConfig.Internals.FULLBRIGHT_ENABLED.getBooleanValue() ? "on" : "off"), "Fullbright"));
                return true;
            } else if (key == BlanketConfig.Keybinds.RELOAD_SOUND.getKeybind()) {
                SoundSystemAccessor soundSystemAccessor = (SoundSystemAccessor) client.getSoundManager();
                soundSystemAccessor.getSoundSystem().reloadSounds();
                client.inGameHud.getChatHud().addMessage((new LiteralText("")).append((new TranslatableText("[Blanket]")).formatted(Formatting.RED, Formatting.BOLD)).append(" ").append("Reloaded sound system"));
            } else if (key == BlanketConfig.Autowalk.AUTOWALK_KEY.getKeybind()) {
                BlanketConfig.Internals.AUTOWALK_ENABLED.setBooleanValue(!BlanketConfig.Internals.AUTOWALK_ENABLED.getBooleanValue());

                if(BlanketConfig.Internals.AUTOWALK_ENABLED.getBooleanValue()) {
                    InfoUtils.printActionbarMessage(StringUtils.translate("blanket.autowalk.started"));
                } else {
                    AutowalkModule.resetPressedKeys(client);
                    InfoUtils.printActionbarMessage(StringUtils.translate("blanket.autowalk.stopped"));
                }
            } else if (key == BlanketConfig.Autowalk.AUTOMINE_KEY.getKeybind()) {
                BlanketConfig.Internals.AUTOMINE_ENABLED.setBooleanValue(!BlanketConfig.Internals.AUTOMINE_ENABLED.getBooleanValue());

                if(BlanketConfig.Internals.AUTOMINE_ENABLED.getBooleanValue()) {
                    InfoUtils.printActionbarMessage(StringUtils.translate("blanket.automine.on"));
                } else {
                    AutowalkModule.resetPressedKeys(client);
                    InfoUtils.printActionbarMessage(StringUtils.translate("blanket.automine.off"));
                }
            }

            return false;
        }
    }
}
