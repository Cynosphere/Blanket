package pm.c7.utils.event;

import com.mojang.blaze3d.systems.RenderSystem;
import fi.dy.masa.malilib.interfaces.IRenderer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.math.MatrixStack;
import pm.c7.utils.BlanketConfig;
import pm.c7.utils.hud.*;

public class RenderHandler implements IRenderer {
    private static final RenderHandler INSTANCE = new RenderHandler();

    private MinecraftClient client;

    public RenderHandler() {
        this.client = MinecraftClient.getInstance();
    }

    public static RenderHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void onRenderGameOverlayPost(float partialTicks, MatrixStack matrixStack) {
        if (!this.client.options.debugEnabled && this.client.player != null) {
            if (BlanketConfig.Features.POTION_HUD.getBooleanValue())
                PotionHUD.INSTANCE.render(matrixStack);

            if (BlanketConfig.Features.ARMOR_HUD.getBooleanValue())
                ArmorHUD.INSTANCE.render(matrixStack);

            if (BlanketConfig.Features.HOTBAR_ENCHANTS.getBooleanValue())
                HotbarEnchantments.INSTANCE.render(matrixStack);

            if (BlanketConfig.Features.COMPASS_HUD.getBooleanValue() && !this.client.options.keyPlayerList.isPressed())
                CompassHUD.INSTANCE.render(matrixStack);

            if (BlanketConfig.Features.PICKUP_LOG.getBooleanValue()) {
                float scale = BlanketConfig.PickupLog.getGuiScale();
                RenderSystem.pushMatrix();
                RenderSystem.scalef(scale, scale, 1);
                PickupLog.INSTANCE.drawItemPickupLog(scale, false, matrixStack);
                RenderSystem.popMatrix();
            }
        }
    }
}
