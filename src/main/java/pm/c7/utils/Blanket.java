package pm.c7.utils;

import fi.dy.masa.malilib.event.InitializationHandler;
import net.fabricmc.api.ClientModInitializer;

public class Blanket implements ClientModInitializer {
    public static final String MOD_ID = "blanket";
    public static final String MOD_NAME = "Blanket";

    public static String lastIp;
    public static int lastPort;

    @Override
    public void onInitializeClient() {
        InitializationHandler.getInstance().registerInitializationHandler(new InitHandler());
    }
}