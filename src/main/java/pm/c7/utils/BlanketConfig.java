package pm.c7.utils;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import fi.dy.masa.malilib.config.ConfigUtils;
import fi.dy.masa.malilib.config.IConfigHandler;
import fi.dy.masa.malilib.config.IConfigValue;
import fi.dy.masa.malilib.config.options.*;
import fi.dy.masa.malilib.util.FileUtils;
import fi.dy.masa.malilib.util.JsonUtils;
import net.minecraft.client.MinecraftClient;
import net.minecraft.util.math.MathHelper;
import pm.c7.utils.hud.helper.CoordsPair;
import pm.c7.utils.hud.helper.Enums;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.List;

public class BlanketConfig implements IConfigHandler {
    private static final String CONFIG_FILE_NAME = Blanket.MOD_ID + ".json";
    private static final int CONFIG_VERSION = 1;

    public static class Features {
        public static final ConfigBoolean TAB_LIST_PING         = new ConfigBoolean("tabListPing", true, "Shows numerical ping in tab list");
        public static final ConfigBoolean PICKUP_LOG            = new ConfigBoolean("pickupLog", true, "Logging of pickups and drops from inventory");
        public static final ConfigBoolean ANTI_BLINDNESS        = new ConfigBoolean("antiBlindness", false, "Disables blindness effects");
        public static final ConfigBoolean POTION_HUD            = new ConfigBoolean("potionHud", true, "Lists potions in bottom right");
        public static final ConfigBoolean ARMOR_HUD             = new ConfigBoolean("armorHud", true, "Displays equipped armor above hunger/oxygen");
        public static final ConfigBoolean HOTBAR_ENCHANTS       = new ConfigBoolean("hotbarEnchants", true, "Displays enchantments and effects of held item/potion above the hotbar");
        public static final ConfigBoolean COMPASS_HUD           = new ConfigBoolean("compassHud", false, "Compass at top of screen");
        public static final ConfigBoolean NO_EFFECT_HUD         = new ConfigBoolean("noEffectHud", true, "Hides top right effect HUD");
        public static final ConfigBoolean NO_BOSS_BAR           = new ConfigBoolean("noBossBar", false, "Hides boss bar");
        public static final ConfigBoolean NO_PUMPKIN_OVERLAY    = new ConfigBoolean("noPumpkinOverlay", false, "Hides pumpkin overlay");
        public static final ConfigBoolean NO_SCOREBOARD_NUMBERS = new ConfigBoolean("noScoreboardNumbers", false, "Hides scoreboard numbers");
        public static final ConfigBoolean NO_SCOREBOARD         = new ConfigBoolean("noScoreboard", false, "Hides scoreboard entirely");
        public static final ConfigBoolean COMPASS_RAINBOW       = new ConfigBoolean("compassRainbow", false, "Rainbow coloring");
        public static final ConfigInteger COMPASS_RAINBOW_SPEED = new ConfigInteger("compassRainbowSpeed", 2, 1, 10, "Rainbow speed");
        public static final ConfigInteger COMPASS_HUE           = new ConfigInteger("compassHue", 0, 0, 360, "Hue value of the compass color");
        public static final ConfigInteger COMPASS_SATURATION    = new ConfigInteger("compassSaturation", 0, 0, 100, "Saturation value of the compass color, also used in rainbow");
        public static final ConfigInteger COMPASS_VALUE         = new ConfigInteger("compassValue", 100, 0, 100, "Value/Brightness/Lightness value of the compass color, also used in rainbow");

        public static final ImmutableList<IConfigValue> OPTIONS = ImmutableList.of(
                TAB_LIST_PING,
                PICKUP_LOG,
                ANTI_BLINDNESS,

                POTION_HUD,
                ARMOR_HUD,
                HOTBAR_ENCHANTS,
                COMPASS_HUD,

                NO_EFFECT_HUD,
                NO_BOSS_BAR,
                NO_PUMPKIN_OVERLAY,
                NO_SCOREBOARD_NUMBERS,
                NO_SCOREBOARD,

                COMPASS_RAINBOW,
                COMPASS_RAINBOW_SPEED,
                COMPASS_HUE,
                COMPASS_SATURATION,
                COMPASS_VALUE
        );
    }

    public static class Titlebar {
        public static final ConfigBoolean CLASSIC_MODE     = new ConfigBoolean("classicMode", false, "Reverts titlebar to pre-1.15 state");
        public static final ConfigBoolean ENHANCED_MODE    = new ConfigBoolean("enhancedMode", true, "Replaces \"Third-Party\" with the server list entry name");
        public static final ConfigBoolean ENHANCED_SHOW_IP = new ConfigBoolean("enhancedShowIP", false, "Shows server IP address after server name");
        public static final ConfigBoolean HIDE_MODDED      = new ConfigBoolean("hideModded", false, "Hides modded indicator (*)");

        public static final ImmutableList<IConfigValue> OPTIONS = ImmutableList.of(
                CLASSIC_MODE,
                ENHANCED_MODE,
                ENHANCED_SHOW_IP,
                HIDE_MODDED
        );
    }

    public static class Autowalk {
        public static final ConfigBoolean FORCE_SPRINT = new ConfigBoolean("forceSprint", true, "Force sprinting when autowalking");
        public static final ConfigHotkey AUTOWALK_KEY  = new ConfigHotkey("autowalk", "I", "Autowalk Toggle", "Autowalk Toggle");
        public static final ConfigHotkey AUTOMINE_KEY  = new ConfigHotkey("automine", "O", "Autowalk Mining Mode Toggle", "Mining Mode Toggle");

        public static final ImmutableList<IConfigValue> OPTIONS = ImmutableList.of(
                FORCE_SPRINT,
                AUTOWALK_KEY,
                AUTOMINE_KEY
        );

        public static final List<ConfigHotkey> HOTKEY_LIST = ImmutableList.of(
                AUTOWALK_KEY,
                AUTOMINE_KEY
        );
    }

    public static class Keybinds {
        public static final ConfigHotkey OPEN_CONFIG_GUI = new ConfigHotkey("openConfigGui", "B,C", "Keybind to open this GUI");
        public static final ConfigHotkey FULLBRIGHT      = new ConfigHotkey("fullbright", "F6", "Fullbright Key", "Fullbright");
        public static final ConfigHotkey RELOAD_SOUND    = new ConfigHotkey("reloadSound", "PAUSE", "Reloads the sound system", "Reload Sound System");

        public static final ImmutableList<IConfigValue> OPTIONS = ImmutableList.of(
                OPEN_CONFIG_GUI,

                FULLBRIGHT,
                RELOAD_SOUND
        );

        public static final List<ConfigHotkey> HOTKEY_LIST = ImmutableList.of(
                OPEN_CONFIG_GUI,

                FULLBRIGHT,
                RELOAD_SOUND
        );
    }

    public static class PickupLog {
        public static final ConfigInteger POS_X           = new ConfigInteger("posX", 86, "");
        public static final ConfigInteger POS_Y           = new ConfigInteger("posY", 17, "");
        public static final ConfigDouble SCALE            = new ConfigDouble("scale", 0.11F, "");
        public static final ConfigOptionList ANCHOR_POINT = new ConfigOptionList("anchorPoint", Enums.AnchorPoint.TOP_LEFT, "");

        private final static float GUI_SCALE_MINIMUM = 0.5F;
        private final static float GUI_SCALE_MAXIMUM = 5;
        private final static float GUI_SCALE_STEP = 0.1F;

        public static Enums.AnchorPoint getAnchorPoint() {
            return (Enums.AnchorPoint) ANCHOR_POINT.getOptionListValue();
        }

        public static CoordsPair getRelativeCoords() {
            return new CoordsPair(POS_X.getIntegerValue(), POS_Y.getIntegerValue());
        }

        public static int getActualX() {
            int maxX = MinecraftClient.getInstance().getWindow().getScaledWidth();
            return getAnchorPoint().getX(maxX) + getRelativeCoords().getX();
        }

        public static int getActualY() {
            int maxY = MinecraftClient.getInstance().getWindow().getScaledHeight();
            return getAnchorPoint().getY(maxY) + getRelativeCoords().getY();
        }

        public static void setCoords(int x, int y) {
            POS_X.setIntegerValue(x);
            POS_Y.setIntegerValue(y);
        }

        public static float getGuiScale() {
            return getGuiScale(true);
        }

        public static float getGuiScale(boolean denormalized) {
            float value = (float) SCALE.getDoubleValue();
            if (denormalized) value = denormalizeScale(value);
            return value;
        }

        public static void setGuiScale(float val) {
            SCALE.setDoubleValue(val * 0.11F);
        }

        private static float denormalizeScale(float value) {
            return snapToStepClamp(GUI_SCALE_MINIMUM + (GUI_SCALE_MAXIMUM - GUI_SCALE_MINIMUM) *
                    MathHelper.clamp(value, 0.0F, 1.0F));
        }
        private static float snapToStepClamp(float value) {
            value = GUI_SCALE_STEP * (float) Math.round(value / GUI_SCALE_STEP);
            return MathHelper.clamp(value, GUI_SCALE_MINIMUM, GUI_SCALE_MAXIMUM);
        }

        public static void setClosestAnchorPoint() {
            int x1 = getActualX();
            int y1 = getActualY();
            int maxX = MinecraftClient.getInstance().getWindow().getScaledWidth();
            int maxY = MinecraftClient.getInstance().getWindow().getScaledHeight();
            double shortestDistance = -1;
            Enums.AnchorPoint closestAnchorPoint = Enums.AnchorPoint.BOTTOM_MIDDLE; // default
            for (Enums.AnchorPoint point : Enums.AnchorPoint.values()) {
                double distance = Point2D.distance(x1, y1, point.getX(maxX), point.getY(maxY));
                if (shortestDistance == -1 || distance < shortestDistance) {
                    closestAnchorPoint = point;
                    shortestDistance = distance;
                }
            }
            int targetX = getActualX();
            int targetY = getActualY();
            int x = targetX - closestAnchorPoint.getX(maxX);
            int y = targetY - closestAnchorPoint.getY(maxY);
            ANCHOR_POINT.setOptionListValue(closestAnchorPoint);
            setCoords(x, y);
        }

        public static final ImmutableList<? extends ConfigBase<? extends ConfigBase<?>>> OPTIONS = ImmutableList.of(
                POS_X,
                POS_Y,
                SCALE,
                ANCHOR_POINT
        );
    }

    public static class Internals {
        public static final ConfigBoolean AUTOWALK_ENABLED = new ConfigBoolean("autowalk", false, "");
        public static final ConfigBoolean AUTOMINE_ENABLED = new ConfigBoolean("automine", false, "");
        public static final ConfigBoolean FULLBRIGHT_ENABLED = new ConfigBoolean("fullbright", false, "");

        public static final ImmutableList<IConfigValue> OPTIONS = ImmutableList.of(
                AUTOWALK_ENABLED,
                AUTOMINE_ENABLED,

                FULLBRIGHT_ENABLED
        );
    }

    public static void loadFromFile()
    {
        File configFile = new File(FileUtils.getConfigDirectory(), CONFIG_FILE_NAME);

        if (configFile.exists() && configFile.isFile() && configFile.canRead())
        {
            JsonElement element = JsonUtils.parseJsonFile(configFile);

            if (element != null && element.isJsonObject())
            {
                JsonObject root = element.getAsJsonObject();
                JsonObject objInfoLineOrders = JsonUtils.getNestedObject(root, "InfoLineOrders", false);

                ConfigUtils.readConfigBase(root, "Features", BlanketConfig.Features.OPTIONS);
                ConfigUtils.readConfigBase(root, "Titlebar", BlanketConfig.Titlebar.OPTIONS);
                ConfigUtils.readConfigBase(root, "PickupLog", BlanketConfig.PickupLog.OPTIONS);
                ConfigUtils.readConfigBase(root, "Autowalk", BlanketConfig.Autowalk.OPTIONS);
                ConfigUtils.readConfigBase(root, "Keybinds", BlanketConfig.Keybinds.OPTIONS);
                ConfigUtils.readConfigBase(root, "Internals", BlanketConfig.Internals.OPTIONS);

                int version = JsonUtils.getIntegerOrDefault(root, "config_version", 0);
            }
        }
    }

    public static void saveToFile()
    {
        File dir = FileUtils.getConfigDirectory();

        if ((dir.exists() && dir.isDirectory()) || dir.mkdirs())
        {
            JsonObject root = new JsonObject();
            JsonObject objInfoLineOrders = JsonUtils.getNestedObject(root, "InfoLineOrders", true);

            ConfigUtils.writeConfigBase(root, "Features", BlanketConfig.Features.OPTIONS);
            ConfigUtils.writeConfigBase(root, "Titlebar", BlanketConfig.Titlebar.OPTIONS);
            ConfigUtils.writeConfigBase(root, "PickupLog", BlanketConfig.PickupLog.OPTIONS);
            ConfigUtils.writeConfigBase(root, "Autowalk", BlanketConfig.Autowalk.OPTIONS);
            ConfigUtils.writeConfigBase(root, "Keybinds", BlanketConfig.Keybinds.OPTIONS);
            ConfigUtils.writeConfigBase(root, "Internals", BlanketConfig.Internals.OPTIONS);

            root.add("config_version", new JsonPrimitive(CONFIG_VERSION));

            JsonUtils.writeJsonToFile(root, new File(dir, CONFIG_FILE_NAME));
        }
    }

    @Override
    public void load()
    {
        loadFromFile();
    }

    @Override
    public void save()
    {
        saveToFile();
    }
}
