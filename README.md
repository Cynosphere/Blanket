# Blanket
Clientside utilities

## Current Features
**(Almost everything is configurable)**

* Sound system reload keybind
* Fullbright
* ~~Enchant glint coloring~~ **Color embedded into the texture in 1.15.x, just modify via resource pack**
* Verbose status effect HUD
* Armor durability on HUD
* Numeric ping on tab list
* Held item enchantment/potion info
* Compass HUD (ripped from Jello client leaked source)
* Hiding vanilla elements
    * Boss bar
    * Status effect icons
    * Scoreboard
    * Scoreboard numbers

## WIP/Planned
* Ghost block revealer (WIP, broken)

## Dependencies
* Fabric Loader + Fabric API
* AutoConfig (included)
* Cloth Config 2 (included)
* ModMenu (or else you have to manually edit configs)